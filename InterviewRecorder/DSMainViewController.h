//
//  DSMainWindow.h
//  InterviewRecorder
//
//  Created by Benjamin Bader on 1/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MobileCoreServices/UTCoreTypes.h>
#import <MediaPlayer/MediaPlayer.h>

@interface DSMainViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

-(IBAction) chooseVideo;

@property (retain) IBOutlet UIProgressView *progressBar;
@property (retain) IBOutlet UIButton *buttonChooseVideo;

@end
