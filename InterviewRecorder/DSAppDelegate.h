//
//  DSAppDelegate.h
//  InterviewRecorder
//
//  Created by Benjamin Bader on 1/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSMainViewController.h"

@interface DSAppDelegate : NSObject <UIApplicationDelegate>
{
    UIWindow *window;
}

@property (retain) DSMainViewController *viewController;

@end
