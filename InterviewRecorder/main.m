//
//  main.m
//  InterviewRecorder
//
//  Created by Benjamin Bader on 1/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, nil); //NSStringFromClass([DSAppDelegate class]));
    }
}
