//
//  DSMainWindow.m
//  InterviewRecorder
//
//  Created by Benjamin Bader on 1/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DSMainViewController.h"

#import "GData.h"
#import "GDataServiceGoogleYouTube.h"
#import "GDataEntryYouTubeUpload.h"

#import "Constants.h"

static NSString *kDSMainWindowProgressNotification = @"DSMainWindowUploadMadeProgress";
static NSString *kDSMainWindowUploadCompleted = @"DSMainWindowUploadCompleted";

@interface DSMainViewController ()

-(void) uploadVideo:(NSString*)path;
-(void) uploadFinished:(NSNotification*)notification;
-(void) progressMade:(NSNotification*)notification;

-(GDataServiceGoogleYouTube*) youTube;

@property (retain) NSURL *uploadLocationUrl;
@property (retain) GDataServiceTicket *uploadTicket;

@end

@implementation DSMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(void) dealloc
{
    self.progressBar = nil;
    self.uploadLocationUrl = nil;
    self.uploadTicket = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDSMainWindowProgressNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDSMainWindowUploadCompleted object:nil];
    
    [super dealloc];
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void) awakeFromNib
{
   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - PrivateMethods Implementation

-(void) uploadVideo:(NSString *)filePath
{
    GDataServiceGoogleYouTube *service = [self youTube];
    
    [service setUserCredentialsWithUsername:@"reachoo" password:@"reachyou0930"];
    
    NSURL *url = [GDataServiceGoogleYouTube youTubeUploadURLForUserID:kGDataServiceDefaultUser];
    
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:filePath];
    NSString *fileName = [filePath lastPathComponent];
    NSString *mimeType = [GDataUtilities MIMETypeForFileAtPath:filePath defaultMIMEType:@"video/mp4"];
    
    NSString *title = fileName;
    GDataMediaTitle *mediaTitle = [GDataMediaTitle textConstructWithString:title];
    
    GDataYouTubeMediaGroup *mediaGroup = [GDataYouTubeMediaGroup mediaGroup];
    
    mediaGroup.mediaTitle = mediaTitle;
    mediaGroup.isPrivate = NO;
    
    GDataEntryYouTubeUpload *entry = [GDataEntryYouTubeUpload uploadEntryWithMediaGroup:mediaGroup
                                                                             fileHandle:fileHandle
                                                                               MIMEType:mimeType
                                                                                   slug:fileName];
    
    service.serviceUploadProgressHandler = ^(GDataServiceTicketBase *ticket, unsigned long long bytesRead, unsigned long long bytesTotal)
    {
        float progress = (float)bytesRead / (float)bytesTotal;
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:progress] forKey:@"progress"];
        [[NSNotificationCenter defaultCenter] postNotificationName:kDSMainWindowProgressNotification object:nil userInfo:userInfo];
    };

    GDataServiceTicket *ticket = [service fetchEntryByInsertingEntry:entry
                                                          forFeedURL:url
                                                   completionHandler:^(GDataServiceTicket *ticket, GDataEntryBase *entry, NSError *error) {
                                                       NSLog(@"Upload finished!");
                                                       
                                                       NSArray *vals = [NSArray arrayWithObjects:entry, error, nil];
                                                       NSArray *keys = [NSArray arrayWithObjects:@"entry", @"error", nil];
                                                       NSDictionary *userInfo = [NSDictionary dictionaryWithObjects:vals forKeys:keys];
                                                       
                                                       [[NSNotificationCenter defaultCenter] postNotificationName:kDSMainWindowUploadCompleted
                                                                                                           object:nil
                                                                                                         userInfo:userInfo];
                                                   }];
    
    self.uploadTicket = ticket;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(progressMade:) name:kDSMainWindowProgressNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadFinished:) name:kDSMainWindowUploadCompleted object:nil];
}

-(void) progressMade:(NSNotification *)notification
{
    NSLog(@"Received progress notification.");
    
    NSDictionary *userInfo = notification.userInfo;
    NSNumber *progressNumber = [userInfo objectForKey:@"progress"];
    
    [self.progressBar setProgress:[progressNumber floatValue] animated:YES];
}

-(void) uploadFinished:(NSNotification *)notification
{
    
    NSDictionary *userInfo = [notification userInfo];
    NSError *error = [userInfo objectForKey:@"error"];
    GDataEntryYouTubeVideo *entry = [userInfo objectForKey:@"entry"];
    
    if (error)
    {
        // report error
        NSString *message = [error localizedDescription];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
        [alert release];
    }
    else
    {
        
        // TODO: Report success!
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                        message:@"Upload to YouTube complete!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
        [alert release];
        
        // TODO: DO NOT IGNORE THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // TODO: Post to app!
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDSMainWindowProgressNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDSMainWindowUploadCompleted object:nil];
}

-(GDataServiceGoogleYouTube*) youTube
{
    static GDataServiceGoogleYouTube *service = nil;
    
    if (!service)
    {
        service = [[GDataServiceGoogleYouTube alloc] init];
        
        service.shouldCacheResponseData = YES;
        service.serviceShouldFollowNextLinks = YES;
        service.isServiceRetryEnabled = YES;
    }
    
    [service setYouTubeDeveloperKey:[NSString stringWithCString:devkey encoding:NSUTF8StringEncoding]];
    
    return service;
}

-(void) chooseVideo
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.mediaTypes = [NSArray arrayWithObject:(NSString*)kUTTypeMovie];
    
    NSArray *sourceTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    
    if (![sourceTypes containsObject:(NSString*)kUTTypeMovie])
    {
        NSLog(@"No video!");
    }
    else
    {
        [self presentModalViewController:picker animated:YES];
    }
    
    [picker release];
}

#pragma mark - UIImagePickerControllerDelegate Implementation

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"Image picker did select some media.");
    
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    NSLog(@"Media type: %@", type);
    
    if ([type isEqualToString:(NSString*)kUTTypeMovie] || [type isEqualToString:(NSString*)kUTTypeVideo])
    {
        NSLog(@"Selected media is a video; uploading it now.");
        
        NSURL *videoUrl = [info objectForKey:UIImagePickerControllerMediaURL];
        [self uploadVideo:videoUrl.absoluteString];
    }
}

@synthesize uploadLocationUrl;
@synthesize uploadTicket;
@synthesize progressBar;
@synthesize buttonChooseVideo;

@end
