//
//  Constants.h
//  InterviewRecorder
//
//  Created by Benjamin Bader on 1/28/12.
//  Copyright (c) 2012 Design Drop, Inc. All rights reserved.
//

#ifndef InterviewRecorder_Constants_h
#define InterviewRecorder_Constants_h

#define HACK_D_URL @"http://localhost:3000"

#define INTERVIEW_URL HACK_D_URL @"/interviews"
#define INTERVIEW_URL_POST INTERVIEW_URL @"/"

#define YOUTUBE_URL @"http://www.youtube.com/"

#define YOUTUBE_CLIENT_ID @"reafchyou0930"
#define YOUTUBE_DEV_KEY @"AI39si7wCwt1cPeXsR3oSDiipkfsZo5_qoOYhFR5DzKvJmYbaRiRFMOMp70mYd9WSKSQbgv8aTXPzkcund2NvC_phTjNF0AFxQ"

const char *clientId;
const char *devkey;

#endif
